# Setting up Portainer with docker-compose

Docker For Windows: Remember to tick the box in Docker > Settings > General:“Expose daemon on tcp://localhost:2375 without TLS”.

### How to start
- Run the script "docker-compose.up.bat"

- Navigate to [http://localhost:9000]