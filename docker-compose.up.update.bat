﻿
cd /d %~dp0

REM *** Requirements ***
REM Docker For Windows: Remember to tick the box in Docker > Settings > General:“Expose daemon on tcp://localhost:2375 without TLS”.
REM Docker For Windows: Share the drives in Docker > Settings > Shared drives

REM Configure Docker to become a single node Swarm
docker swarm init

docker-compose -f portainer-agent-stack.yml pull

docker stack rm portainer
docker stack deploy -c portainer-agent-stack.yml portainer
pause
